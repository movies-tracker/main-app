const moviesArr = JSON.parse(document.querySelector("#movies-array").dataset.movies);
if (!Array.isArray(moviesArr)) {
    alert("Unexpected result from server. Please contact admin or refresh page");
    throw new Error("Undefined data");
}
const moviesTemplate = document.querySelector("#movie-template");
const moviesContainer = document.querySelector("#movies-container");
const row = moviesContainer.querySelector("#all-movies-row");

let colClasses = "col-md-6 col-xs-6 col-sm-4 col-lg-4 mt-3 mb-3".split(" ");

for (let i = 0; i < moviesArr.length; i++) {
    let movie = moviesArr[i];
    let col = document.createElement("col");
    colClasses.forEach(cls => col.classList.add(cls));

    let movieElement = moviesTemplate.cloneNode(true);
    movieElement.id = idifyTitle(movie.title);
    movieElement.querySelector(".card-title").innerHTML = movie.title;
    movieElement.classList.remove("d-none");
    let img = movieElement.querySelector("img.card-img-top");
    img.src = movie.posterUrl;
    img.alt = `${movie.title} URL`;
    movieElement.querySelector("p.card-text").innerHTML = movie.plot;
    movieElement.querySelector("a.btn.btn-primary").href = `movie/${movie.imdbID}`;

    col.appendChild(movieElement);
    row.appendChild(col);
    moviesContainer.appendChild(row);
}

function idifyTitle(str) {
    // Create an id like so - "A new string" = a-new-string
    str = str.toLowerCase();
    return str.replace(/\s/g, "-");
}