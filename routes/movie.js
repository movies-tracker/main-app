const express = require("express");
const router = express.Router();
const axios = require("axios").default;
const createError = require('http-errors');

const { moviesService } = require("../helpers/constants");

router.get("/:id", function (req, res, next) {
    let movieId = req.params.id;
    if (!movieId) {
        console.error("No ID defined");
        return next(createError(400));
    }
    axios.get(`${moviesService}/imdb/${movieId}`).then(function (response) {
        if (response.status != 200) {
            console.error("Non-OK response received. Code: " + response.status + " error: " + response.data);
            return next(createError(response.status, response.data));
        }
        return res.render("single_movie.ejs", { data: response.data });
    }).catch(function (err) {
        console.error(err);
        return next(createError(500, err));
    })
});

module.exports = router;