var express = require('express');
var router = express.Router();
const { moviesService } = require("../helpers/constants");
const axios = require("axios").default;

/* GET home page. */
router.get('/', function (req, res, next) {
  // Issue a search for the movies URL
  // Render index and forward the details
  let url = `${moviesService}`
  axios.get(url).then(result => {
    if (result.status != 200) {
      let error = new Error(result.data);
      error.statusCode = result.status;
      console.log(result);
      return next(error);
    }
    // Create the data array
    let data = result.data;
    // Return the results
    return res.render("index", { data });
  }).catch(err => {
    let error = new Error(err.message);
    error.statusCode = 500;
    console.log(err);
    return next(error);
  });
});

module.exports = router;
