# Frontend for movies tracker

This repository is used for the frontend of the Movies tracker website. It consumes data from an external API and displays the relevant
movies.
